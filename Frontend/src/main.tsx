import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
// import { createMemoryRouter, RouterProvider } from "react-router";

import Home from "./pages/frontPage/Home.tsx";
import Features from "./pages/frontPage/Features.tsx";
import Contact from "./pages/frontPage/Contact.tsx";
import Info from "./pages/frontPage/Info.tsx";
import Login from "./pages/frontPage/Login.tsx";
import Register from "./pages/frontPage/Register.tsx";
import Profile from "./pages/frontPage/Profile.tsx";
import App from "./pages/App.tsx";
import { store } from "./redux/store.tsx";
import { Provider } from "react-redux";

// Pretendo usar MemoryBrowserRouter no final do projeto.
const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/features",
    element: <Features />,
  },
  {
    path: "/info",
    element: <Info />,
  },
  {
    path: "/contact",
    element: <Contact />,
  },
  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/register",
    element: <Register />,
  },
  {
    path: "/profile",
    element: <Profile />,
  },
  {
    path: "/app",
    element: <App />,
  },
]);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);
