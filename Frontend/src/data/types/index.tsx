import { IconType } from "react-icons";
import React from "react";

interface Graph {
  day: string;
  interactions: number;
}

interface Card {
  Icon: IconType;
  title: string;
  children: React.ReactNode;
  important?: boolean;
  route?: string;
}

interface User {
  name: string;
  avatar: string;
  online: boolean;
  role?: string;
  school?: string;
}

interface Message {
  id: number;
  avatar: string;
  name: string;
  role: string;
  school: string;
  message: string;
}

interface Event {
  id: number;
  title: string;
  date: Date;
  color: "blue" | "green" | "yellow" | "red";
}

interface Task {
  id: number;
  title: string;
  description: string;
  status: "pendente" | "em desenvolvimento" | "terminado";
}

interface message_type {
  ID?: number;
  ID_Usuario: number;
  ID_Grupo: number;
  Data: Date;
  Mensagem: string
  Tipo: "Texto" | "Imagem" | "Arquivo";
}

interface Question {
  question: string;
  options: String[];
  answer: number;
}

interface Quiz {
  questions: Question[];
}

export type { Graph, Card, User, Message, Event, Task, message_type, Quiz };
