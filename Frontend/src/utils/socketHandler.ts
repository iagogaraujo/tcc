import * as io from 'socket.io-client';
import {AxiosResponse} from 'axios';
import api from '../services/api';
import {Quiz} from '../data/types';

interface Message {
    ID?: number;
    ID_Usuario: number;
    ID_Grupo: number;
    Data: Date;
    Mensagem: string
    Tipo: "Texto" | "Imagem" | "Arquivo";
}

interface Callback<T> {
    (arg0: T): void;
}

interface TokenResponse {
    token: string;
    status: number;
    data: { token: string };
}

const CONNECT = 'connect';
const CONNECT_ERROR = 'connect_error';  // TODO: Implementar posteriormente
const JOIN = 'join';
const RECEIVE_MESSAGE = 'receive-message';
const GET_MESSAGES = 'get-messages';
const MESSAGE = 'message';
const RECEIVE_QUIZ = 'receive-quiz';
// const RECEIVE_EXPLANATION = 'receive-explanation';

class ChatSocketHandler {
    private static socket: io.Socket;
    private static token: string;

    private static id: number;
    private static refreshToken: string;
    private static onRefreshToken: Callback<string>;
    private static onDisconnect: Callback<string>;

    public static initialize(id: number, refreshToken: string, onRefreshToken: Callback<string>, onDisconnect: Callback<string>, token: string) {
        ChatSocketHandler.id = id;
        ChatSocketHandler.refreshToken = refreshToken;
        ChatSocketHandler.onRefreshToken = onRefreshToken;
        ChatSocketHandler.onDisconnect = onDisconnect;
        ChatSocketHandler.token = token;
        ChatSocketHandler.socket = ChatSocketHandler.createSocket();
        ChatSocketHandler.setSocketCallbacks();
    }

    public static onMessage(callback: Callback<Message>): void {
        if (this.socket.hasListeners(RECEIVE_MESSAGE)) {
            console.log('Já existe um listener para RECEIVE_MESSAGE');
            return;
        }
        this.socket.on(RECEIVE_MESSAGE, callback);
        // this.socket.once(RECEIVE_MESSAGE, callback);
    }

    public static offMessage(callback: Callback<Message>): void {
        this.socket.off(RECEIVE_MESSAGE, callback);
    }

    public static async getMessages(callback: Callback<Message[]>): Promise<void> {
        this.socket.on(GET_MESSAGES, callback);
        this.socket.emit(GET_MESSAGES);
    }

    public static sendMessage(message: Message): void {

        this.socket.emit(MESSAGE, message);
    }

    public static onQuiz(callback: Callback<Quiz>): void {
        console.log("Definindo onQuiz...")
        this.socket.on(RECEIVE_QUIZ, callback);
    }

    // public static onExplanation(callback: Callback<string>): void {
    //     this.socket.on(RECEIVE_EXPLANATION, callback);
    // }

    public static disconnect(reason: string): void {
        this.socket.disconnect();
        this.onDisconnect(`Socket desconectado: ${reason}`);
    }

    private static createSocket(): io.Socket {
        return io.connect('ws://127.0.0.1:3000/chat', {
            query: {ID_Grupo: this.id},
            auth: {token: this.token}
        });
    }

    private static setSocketCallbacks(): void {
        this.socket.on(CONNECT, this.joinServerGroup.bind(this));
        this.socket.on(CONNECT_ERROR, this.handleConnectionError.bind(this));
    }

    private static joinServerGroup(): void {
        this.socket.emit(JOIN, this.id);
    }

    private static async handleConnectionError(err: any): Promise<void> {
        if (err.message === "Acesso negado. Token expirado.") {
            await this.refreshTokenHandler();
        } else {
            this.disconnect("Erro ao conectar");
        }
    }

    private static async refreshTokenHandler(): Promise<void> {
        const response: AxiosResponse<TokenResponse> = await api.post('/usuario/refresh', {
            refreshToken: this.refreshToken,
        });

        if (response.status !== 200) {
            this.disconnect('Erro ao atualizar token');
            return;
        }

        this.token = response.data.token;
        this.onRefreshToken(this.token);
    }
}

export default ChatSocketHandler;
