import TodoText from "../../components/generic/TodoText"

function Settings() {
  return (
    <TodoText text="Settings" />
  )
}

export default Settings