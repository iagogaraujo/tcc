// TODO: Mensagem enviada pelo usuário está sendo recebida por uma mensagem enviada pelo socket. O que pode ser problemático.

import React, { useEffect, useLayoutEffect, useState } from "react";
import { MdOutlineGroups } from "react-icons/md";

import MenuTitle from "../../components/generic/MenuTitle";
import ChatGroupInfo from "../../components/chat/ChatGroupInfo";
import ChatMessage from "../../components/chat/ChatMessage";
import ChatInput from "../../components/chat/ChatInput";
import { message_type, Quiz } from "../../data/types";
import ChatSocketHandler from "../../utils/socketHandler.ts";
import { RootState } from "../../redux/store.tsx";
import { useSelector } from "react-redux";
import QuizModal from "../../components/quiz/QuizModal.tsx";

interface addMessageProps {
  message: message_type,
  messages: message_type[],
  setMessages: React.Dispatch<React.SetStateAction<message_type[]>>
}

const addMessage = ({ message, messages, setMessages }: addMessageProps) => {
  if (messages.some((m) => m.ID === message.ID)) return;
  setMessages((messages) => [...messages, message]);
}

const handleOnMessage = (message: message_type, messages: message_type[], setMessages: React.Dispatch<React.SetStateAction<message_type[]>>) => {
  const newMessage = message as message_type;
  if (messages.filter((m) => m.ID === newMessage.ID).length === 0) addMessage({
    message: newMessage,
    messages,
    setMessages
  });
}

const onRefresh = (newToken: string) => window.localStorage.setItem("x-access-token", newToken);
const onDisconnect = () => window.location.href = "/login";

function Chat() {
  const ID_Grupo = useSelector((state: RootState) => state.chat.group_ID);
  const [message, setMessage] = useState<string>(""); // Input
  const [messages, setMessages] = useState<message_type[]>([]); // Chat
  const token = window.localStorage.getItem("x-access-token") || "";
  const refreshToken = window.localStorage.getItem("x-refresh-token") || "";

  const [quizModal, setQuizModal] = useState<boolean>(false);
  const [quiz, setQuiz] = useState<Quiz | null>(null);

  useEffect(() => {
    ChatSocketHandler.initialize(ID_Grupo, refreshToken, onRefresh, onDisconnect, token);
    ChatSocketHandler.getMessages((messages: message_type[]) => {
      setMessages(messages);
    });
  }, []);

  useEffect(() => {
    const callback = (msg: message_type) => handleOnMessage(msg, messages, setMessages);
    ChatSocketHandler.onMessage(callback);
    ChatSocketHandler.onQuiz((quiz: Quiz) => {
      setQuiz(quiz);
      setQuizModal(true);
    });
    // ChatSocketHandler.onExplanation((explanation: string) => {
    //   console.log(explanation);
    // });
    return () => {
      ChatSocketHandler.offMessage(callback);
    };
  }, [ID_Grupo]);

  useLayoutEffect(() => {
    const chat = document.getElementById("chat");
    if (chat) chat.scrollTop = chat.scrollHeight;
  }, [messages]);

  const sendMessage = (message: message_type) => {
    ChatSocketHandler.sendMessage(message);
    setMessage("");
  };

  return (
    <div className="flex flex-col h-full font-sans">

      {
        quizModal ? <QuizModal setQuizModal={setQuizModal} quiz={quiz} /> : <></>
      }

      <div className="flex flex-row h-full">
        <div className="w-full md:w-3/4 flex flex-col">
          <MenuTitle icon={<MdOutlineGroups />} title={`Chat - Grupo de Teste`}>
            <></>
          </MenuTitle>
          <div className="flex flex-col flex-grow overflow-y-auto p-5 pb-0" id="chat">
            {messages.map((message, index) => (
              <ChatMessage
                key={index}
                message={message.Mensagem}
                type={message.Tipo}
                data={new Date(message.Data).toLocaleTimeString("pt-BR", {
                  hour: "2-digit",
                  minute: "2-digit",
                  second: "2-digit",
                })}
              />
            ))}
          </div>
          <div className="w-full md:p-5">
            <ChatInput
              text={message}
              setMessage={setMessage}
              handleSendMessage={() => sendMessage({
                ID_Usuario: 1,
                ID_Grupo,
                Data: new Date(),
                Mensagem: message,
                Tipo: "Texto",
              })}
            />
          </div>
        </div>
        <div className="hidden md:flex flex-col w-1/4 bg-white border-l-2">
          <div className="h-16 w-full bg-gray-50 border-b-2" />
          <div className="hidden md:block w-full bg-white p-4 ">
            <ChatGroupInfo />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Chat;
