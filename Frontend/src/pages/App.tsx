import Sidebar from "../components/generic/Sidebar";
import Home from "./menus/Home";
import Groups from "./menus/Groups";
import { useState } from "react";
import Calendar from "./menus/Calendar";
import Messages from "./menus/Messages";
import Tasks from "./menus/Tasks";
import Settings from "./menus/Settings";
import Chat from "./menus/Chat";

function App() {
  const [active, setActive] = useState("home");
  const redirect = (name: string) => {
    const validNames = [
      "home",
      "groups",
      "calendar",
      "messages",
      "tasks",
      "settings",
      "chat"
    ];
    if (validNames.includes(name)) {
      setActive(name);
    }
  };
  return (
    <div className="flex flex-row w-screen h-screen">
      <Sidebar active={active} setActive={setActive} />
      <div className="flex flex-col flex-grow bg-gray-50 overflow-y-scroll">
        {
          {
            home: <Home redirect={redirect} />,
            // home: <Chat />,
            groups: <Groups redirect={redirect} />, 
            calendar: <Calendar />,
            messages: <Messages />,
            tasks: <Tasks />,
            settings: <Settings />,
            chat: <Chat />
          }[active]
        }
      </div>
    </div>
  );
}

export default App;
