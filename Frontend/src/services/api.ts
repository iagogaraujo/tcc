import axios from 'axios';
// Obtendo o endereço da variável de ambiente
// const backendURL = process.env.REACT_APP_BACKEND_URL;

export default axios.create({
  baseURL: `http://127.0.0.1:3000/api`,
});
