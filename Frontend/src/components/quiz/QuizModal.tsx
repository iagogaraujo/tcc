import { useState } from 'react';
import Card from "../generic/Card.tsx";
import { Quiz } from "../../data/types";

interface QuizModalProps {
  setQuizModal: React.Dispatch<React.SetStateAction<boolean>>;
  quiz: Quiz | null;
}

interface Question {
  question: string;
  options: string[];
  correctAnswer: number;
  selectedAnswer: number | null;
  isAnswered: boolean;
}

function QuizModal({ setQuizModal, quiz }: QuizModalProps) {
  console.log('printing quiz');
  console.log(quiz);
  // const [quizQuestions, setQuizQuestions] = useState<Question[]>([
  //   {
  //     question: "Qual a cor do cavalo branco de Napoleão?",
  //     options: [
  //       "Branco",
  //       "Preto",
  //       "Marrom",
  //       "Cinza"
  //     ],
  //     correctAnswer: 0,
  //     selectedAnswer: null,
  //     isAnswered: false
  //   },
  //   {
  //     question: "Qual foi o primeiro presidente do Brasil?",
  //     options: [
  //       "Dilma Roussef",
  //       "Jair Bolsonaro",
  //       "Michel Temer",
  //       "Manuel Deodoro da Fonseca"
  //     ],
  //     correctAnswer: 3,
  //     selectedAnswer: null,
  //     isAnswered: false
  //   },
  // ]);
  const [quizQuestions, setQuizQuestions] = useState<Question[]>(
    quiz?.questions.map(question => {
      return {
        question: question.question,
        options: question.options,
        correctAnswer: question.answer,
        selectedAnswer: null,
        isAnswered: false
      };
    }) || []);

  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [showScore, setShowScore] = useState(false);

  const handleAnswer = (answerIndex: number) => {
    setQuizQuestions(prevState => {
      const stateClone = [...prevState];
      stateClone[currentQuestion].selectedAnswer = answerIndex;
      stateClone[currentQuestion].isAnswered = true;
      return stateClone;
    });

    if (currentQuestion < quizQuestions.length - 1) {
      setTimeout(() => {
        setCurrentQuestion(prev => prev + 1);
      }, 2000);
    } else {
      setTimeout(() => {
        setShowScore(true);
      }, 2000);
    }
  };

  let correctCount = 0;
  quizQuestions.forEach(question => {
    if (question.selectedAnswer === question.correctAnswer) {
      correctCount++;
    }
  });

  const question = quizQuestions[currentQuestion];

  return (
    <div
      className="fixed inset-0 bg-black bg-opacity-70 flex justify-center items-center z-50"
    >
      <Card
        className="w-3/4 max-w-lg p-6 bg-white rounded-lg shadow-xl relative"
      >
        {!showScore ? (
          <>
            <h2 className="text-2xl font-bold mb-4 text-gray-900">Quiz</h2>
            <div className="my-4">
              <p className="font-semibold text-gray-700 mb-2">{question?.question}</p>
              {question?.options.map((option, optionIndex) =>
                <button
                  key={optionIndex}
                  className={`block w-full py-2 px-4 mt-2 rounded-lg 
        ${question?.selectedAnswer != null ?
                      question?.correctAnswer === optionIndex ? 'bg-green-500 text-white' :
                        question?.selectedAnswer === optionIndex ? 'bg-red-500 text-white' : 'bg-indigo-100 text-indigo-700'
                      : 'bg-indigo-100 text-indigo-700'} 
        ${question?.isAnswered ? 'opacity-50 cursor-not -allowed' : ''}`}
                  onClick={() => !question?.isAnswered && handleAnswer(optionIndex)}
                  disabled={question?.isAnswered}
                >
                  {option}
                </button>
              )}
              {question?.selectedAnswer != null &&
                <p className="mt-2 text-sm text-gray-500">
                  {question?.selectedAnswer === question?.correctAnswer ? 'Resposta correta!' : 'Resposta incorreta!'}
                </p>}
            </div>
          </>
        ) : (
          <div>
            <p className="text-lg font-semibold text-gray-700">
              Score final: {correctCount} de {quizQuestions.length} perguntas acertadas
            </p>
            <button
              className="mt-4 bg-indigo-500 text-white px-4 py-2 rounded-lg shadow w-full"
              onClick={() => setQuizModal(false)}
            >
              Fechar
            </button>
          </div>
        )}
      </Card>
    </div>
  );
}

export default QuizModal;
