const jwt = require("jsonwebtoken");
const socketIO = require("socket.io");
const events = require("events");
const OpenAI = require('openai');
const { Mensagem } = require("../models");
const { decryptToken } = require("../middleware/auth");
const eventEmitter = new events.EventEmitter();

const openai = new OpenAI({
  apiKey: "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",
});

const corsOptions = {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
};
const filterMessage = (messageText) => messageText.includes("Teste");

const joinRoom = (socket, room) => socket.join(room);

const generateExpĺanation = async (text) => {
  const prompt = `
    Aja como um professor explicando, detalhadamente, a seguinte pergunta do aluno: "${text}".
    Você pode usar exemplos, analogias e comparações para ajudar o aluno a entender.
    Procure ser claro e conciso em sua explicação. Também use markdown para formatar sua resposta quando possível.

    Não faça qualquer menção ao prompt anterior. Apenas responda à pergunta do aluno.
  `;
  const chatCompletion = await openai.chat.completions.create({
    messages: [
      { role: 'system', content: 'You are a teacher explaining a concept to a student.' },
      { role: 'user', content: prompt }],
    model: 'gpt-4',
  });

  const completionText = chatCompletion.choices[0].message.content;
  console.log('completionText', completionText);

  return completionText;
}

const generateQuiz = async (theme) => {


  const prompt = `
    Imagine que você é um professor e está criando um questionário para seus alunos com base no tema "${theme}".
    Cada questionário deve ter 3 perguntas.
    Crie um array JSON válido de objetos com as seguintes propriedades para o questionário:
      [{
        \"question\": \"Pergunta a ser feita aos alunos\",
        \"options\": \"Array de opções para escolher\",
        \"answer\": \"Índice da resposta correta no array de opções. O índice começa em 0.\"
      }]
    Um exemplo de um array JSON válido de objetos para o tema Brasil é:
    O objeto JSON é:
      [{
        \"question\": \"Qual é a capital do Brasil?\",
        \"options\": [\"Rio de Janeiro\", \"Brasília\", \"São Paulo\", \"Belo Horizonte\"],
        \"answer\": 1
      }, ...]
    E assim por diante...

    O objeto JSON do questionário de tema  "${theme}" é:
`;

  const chatCompletion = await openai.chat.completions.create({
    messages: [
      { role: 'system', content: 'You are a teacher creating a quiz for your students.' },
      { role: 'user', content: prompt }],
    // model: 'gpt-3.5-turbo',
    model: 'gpt-4',
  });

  const completionJson = JSON.parse(chatCompletion.choices[0].message.content);
  console.log('completionJson', completionJson);

  return completionJson;
}

const messageHandler = async (socket, message) => {


  if (filterMessage(message.Mensagem)) {
    message = { ...message, Mensagem: "Mensagem bloqueada pela LLM", Tipo: "Texto" };
  }

  if (message.Mensagem.startsWith("/quiz")) {
    console.log("/quiz detectado, enviando quiz ao usuário.");
    // pick all words after /quiz
    const theme = message.Mensagem.split(" ").slice(1).join(" ");

    // await generateQuiz(theme);
    const aiQuestions = await generateQuiz(theme);
    console.log('aiQuestions', aiQuestions);

    const quiz = {
      questions: aiQuestions
    };

    socket.emit("receive-quiz", quiz);
    return;
  }

  if (message.Mensagem.startsWith("/explicar")) {
    console.log("/explicar detectado, enviando explicação ao usuário.");
    // pick all words after /explicar
    const text = message.Mensagem.split(" ").slice(1).join(" ");

    const aiExplanation = await generateExpĺanation(text);

    const returnMessage = {
      ID_Grupo: socket.ID_Grupo,
      ID_Usuario: 1,
      Mensagem: aiExplanation,
      Tipo: "Texto",
      Data: new Date()
    };

    socket.emit("receive-message", returnMessage);
    return;
  }

  eventEmitter.emit("save-message", message);
  // Optimizar para não enviar a mensagem para o usuário que a enviou
  socket.emit("receive-message", message);
  socket.broadcast.emit("receive-message", message);
};

const getMessages = async (socket) => {
  const messages = await Mensagem.findAll({
    where: { ID_Grupo: socket.ID_Grupo }, order: [['Data', 'ASC']]
  });

  socket.emit("get-messages", messages);
};

module.exports = (server) => {
  const io = socketIO(server, corsOptions);
  const chat = io.of("/chat");

  chat.on("connection", (socket) => {
    console.log(`Nova conexão de ${socket.user.id}`);

    socket.emit("connected", socket.user.id);
    socket.on("join", room => joinRoom(socket, room));
    socket.on("message", message => messageHandler(socket, message));
    socket.on("get-messages", () => getMessages(socket));
  });

  chat.use((socket, next) => {
    const { token } = socket.handshake.auth;

    if (!token) return next(new Error("Acesso negado. Token não fornecido."));
    if (!jwt.decode(token) || jwt.decode(token).exp < Date.now() / 1000)
      return next(new Error("Acesso negado. Token inválido ou expirado."));

    socket.user = decryptToken(token);
    next();
  });

  chat.use((socket, next) => {
    const { ID_Grupo } = socket.handshake.query;

    if (!ID_Grupo) return next(new Error("Acesso negado. ID do grupo não fornecido."));

    socket.ID_Grupo = ID_Grupo;
    next();
  });

  eventEmitter.on("save-message", (message) => {
    Mensagem.create(message);
    console.log(`Mensagem salva no banco de dados.`);
  });
};
