"use strict";

module.exports = (sequelize, DataTypes) => {
  const Mensagem = sequelize.define(
    "Mensagem",
    {
      // TODO: Resolver o problema da chave ser definida por um float?????
      // ^ Eu nem sei como isso é possível...
      // Ainda não é um problema, mas pode vir a ser.
      ID: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
      },
      // * Isso vai quebrar tantas partes do código se for removido.......
      Data: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: DataTypes.NOW,
      },
      Mensagem: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      Tipo: {
        type: DataTypes.ENUM("Texto", "Imagem", "Arquivo"),
        allowNull: false,
      },
      ID_Usuario: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Usuario",
          key: "ID",
        },
        onDelete: "CASCADE",
      },
      ID_Grupo: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Grupo",
          key: "ID",
        },
        onDelete: "CASCADE",
      },
    },
    {
      tableName: "Mensagem",
      timestamps: false,
    }
  );

  return Mensagem;
};
